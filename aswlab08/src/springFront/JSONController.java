package springFront;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import models.ModelException;
import models.Tweet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import util.TimeService;


@Controller
public class JSONController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value="/api/tweets", method=RequestMethod.GET, produces="application/json")
	@ResponseBody public Object homePageJson(HttpSession session) {
		/*Map<String,String> result = new HashMap<String,String>();
		result.put("status", "success");
		result.put("content", "It Works!");
		return result;*/
		logger.info("Entering into the main page");
		String userName = (String) session.getAttribute("loggedUserNAME");
		Vector<HashMap<String, Object>> wallTweets = new Vector<HashMap<String, Object>>();
		SimpleDateFormat mySQLTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int deletable = 0;
		try {
			Collection<Tweet> tweets = Tweet.findAll();			
			for (Tweet tweet : tweets) {
				HashMap<String,Object> tweetHash = new HashMap<String, Object>();
				tweetHash.put("tweetID", tweet.getTweet_id());
				tweetHash.put("authorID", tweet.getUser_id());
				tweetHash.put("content", tweet.getText());
				tweetHash.put("likes", tweet.getLikes());
				java.util.Date temps = mySQLTimeStamp.parse(tweet.getTime(), new ParsePosition(0));
				tweetHash.put("tweetDate", TimeService.getDate(temps));
				tweetHash.put("tweetHour", TimeService.getHour(temps));
				String authorName = tweet.getUser_name();
				tweetHash.put("authorName", authorName);
				if (userName !=null && userName.equals(authorName)) deletable++;
				wallTweets.addElement(tweetHash);
			}
			logger.info("Rendering the main page");
			return wallTweets;
		}
		catch (ModelException ex) {
			return "error";
		}
	}
	@RequestMapping(value="/api/tweets/{tweetID}/like", method=RequestMethod.GET, produces="application/json")
	@ResponseBody public Object likeHandler(@PathVariable("tweetID") Integer tweetID, HttpSession session,
			@ModelAttribute("likedTable") HashSet<Integer> table) {

		logger.info("Liking Tweet with id = "+tweetID) ;

		String result = "";
		try {
			
			Tweet tweet = Tweet.findById(tweetID);
			int likes = tweet.getLikes();
			if (!table.contains(tweetID)) {
				likes = likes +1;
				tweet.setLikes(likes);
				tweet.update();
				table.add(tweetID);
			}			
			result = String.valueOf(likes);
			logger.info("Tweet #"+tweetID+"'s likes = "+result) ;
		}
		catch (ModelException ex) {
			logger.info("Tweet inc error: " + ex.getMessageList().get(0)) ;
			session.setAttribute("theList",ex.getMessageList());
			result = "<a href=error.sp>ERROR</a>";
		}
		return result;
	}


	
}
